'use strict';

var path = require('path');
var inputFile = path.join(__dirname, './words.txt');
var outputFile = path.join(__dirname, './output.txt');
var anagramerizer = require('./anagramerizer');
var fs = require('fs');
var chalk = require('chalk');
var _ = require('lodash');
var async = require('async');

async.waterfall([
  function deleteExistingOutput(unlinkCallback) {
    console.log(chalk.yellow.bold('Deleting existing output file if it exists and tossing midgets'));
    fs.exists(outputFile, function (exists) {
      if (exists) {
        fs.unlink(outputFile, unlinkCallback);
      } else {
        unlinkCallback();
      }
    });
  },
  function readWordsFromFile(readFileCallback) {
    console.log(chalk.magenta.bold('Reading words from your stupid file and setting things on fire'));
    fs.readFile(inputFile, readFileCallback);
  },
  function sortIntoAnagramGroups(data, sortCallback) {
    console.log(chalk.cyan.bold('Sorting words into anagram groups and eating bugs'));
    anagramerizer.sortAnagramGroups(data.toString().split('\n'), sortCallback)
  },
  function writeSortedGroups(sortedGroups, writeFileCallback) {
    console.log(chalk.yellow.bold('Writing your petty output file...'));
    var outStream = fs.createWriteStream(outputFile, {'flags': 'a'});
    if (sortedGroups) {
      _.each(sortedGroups, function (val) {
        outStream.write(val.toString().replace(/,/g, ' ') + '\n');
      });
    }
    outStream.end();
    console.log(chalk.green.bold('Done!'));
    writeFileCallback(null);
  }
], function(err) {
  if(err) {
    console.error(chalk.red.bold('An error occured, you probably screwed something up you idiot!'));
    console.error(err);
  }
});

