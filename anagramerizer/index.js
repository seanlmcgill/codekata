var sortWord = function (word) {
  var sortedWord = null;
  if (word) {
    word = word.replace(/ /g, '');
    if (word.length > 0) {
      sortedWord = word.split('').sort().toString();
    }
  }
  return sortedWord;
};

var sortAnagramGroups = function (wordsArray, cb) {
  var anagramGroups = {};
  if (wordsArray) {
    wordsArray.forEach(function (word) {
      var sortedWord = sortWord(word);
      if (sortedWord) {
        if(word.indexOf(' ') > 0) {
          word = '"' + word + '"';
        }
        if (anagramGroups[sortedWord]) {
          anagramGroups[sortedWord].push(word);
        } else {
          anagramGroups[sortedWord] = [];
          anagramGroups[sortedWord].push(word);
        }
      }
    });
    cb(null, anagramGroups);
  } else {
    cb('Nothing to sort you sot!', null);
  }
};

module.exports = {
  sortAnagramGroups: sortAnagramGroups
};